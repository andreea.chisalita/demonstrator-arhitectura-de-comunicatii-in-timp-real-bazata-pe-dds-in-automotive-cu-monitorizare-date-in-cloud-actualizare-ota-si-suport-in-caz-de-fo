#include <thread>
#include "SampleInfo.h"
#include <pigpio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <chrono>

void configurareDDS()
{

	topic2.rec(0);
	topic2.message("DEFAULT VALUE");
	std::cout << "INITIAL Message " << topic2.message() << " cu valoarea flag-ului de recalibrare: " << topic2.rec() << " ;" << std::endl;

	eprosima::fastrtps::ParticipantAttributes participant_attr2;
	Participant *participant2 = Domain::createParticipant(participant_attr2);

	SubscriberAttributes subscriber_attr2;
	
	subscriber_attr2.topic.topicName = "topicRecalibrareTopic";
	subscriber5 = Domain::createSubscriber(participant2, subscriber_attr2);

}

int main(int argc, char** argv)
{
	std::cout << "Starting subscriber." << std::endl;
	int samples = 10;

	if(gpioInitialise() < 0)
	{
		std::cerr << "Failed to initialize GPIO\n";
		return 1;
	}

	gpioSetMode(16, PI_OUTPUT);
	gpioWrite(16, 0);

	configurareDDS();

	while(samples)
		{
		
			if (subscriber5->takeNextData((void*)&topic2, &m_info2))
			{
				if (m_info2.sampleKind == eprosima::fastrtps::rtps::ALIVE)
				{
				std::cout << "Message: " << topic2.message() << " cu variabila flag RECALIBRARE: " << topic2.rec() << "- TRUE" << " RECEIVED" << std::endl;
				
				gpioWrite(16, 1);
					
				std::ofstream fisier5("outputSubscriberRec.txt", std::ios::app);
				if(fisier5.is_open())
				{
					fisier5 << "Message: " << topic2.message() << " cu variabila flag RECALIBRARE: " << topic2.rec() << "- TRUE" << " RECEIVED" << std::endl;
					fisier5.close();
				}
				else
				{
					std::cerr << "Eroare la deschiderea fisierului! " << std::endl;
				}
			}
			}
		
		std::this_thread::sleep_for(std::chrono::milliseconds(10));//100
		gpioWrite(16, 0);
		}


	gpioTerminate();
	return 0;
}
