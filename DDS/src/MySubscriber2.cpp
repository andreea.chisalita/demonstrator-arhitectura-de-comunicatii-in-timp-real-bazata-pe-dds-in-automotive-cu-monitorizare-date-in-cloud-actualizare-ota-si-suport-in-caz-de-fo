#include <thread>
#include "SampleInfo.h"
#include <pigpio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <chrono>

void configurareDDS()
{

	topic1.fail(0);
    topic1.message("DEFAULT VALUE");
	std::cout << "INITIAL Message " << topic1.message() << " cu valoarea variabilei fail: " << topic1.fail() << " ;" << std::endl;

    eprosima::fastrtps::ParticipantAttributes participant_attr1; 
	Participant *participant1 = Domain::createParticipant(participant_attr1);

	SubscriberAttributes subscriber_attr1;
	
	subscriber_attr1.topic.topicName = "topicFailOperationTopic";
	
	subscriber4 = Domain::createSubscriber(participant1, subscriber_attr1);

}

int main(int argc, char** argv)
{
    std::cout << "Starting subscriber." << std::endl;
    int samples = 10;

	 if(gpioInitialise() < 0)
    {
        std::cerr << "Failed to initialize GPIO\n";
        return 1;
    }


    gpioSetMode(16, PI_OUTPUT);

    configurareDDS();
    while(samples)
        {
        
        	if (subscriber4->takeNextData((void*)&topic1, &m_info1))
    		{
         		if (m_info1.sampleKind == eprosima::fastrtps::rtps::ALIVE)
        		{
                    std::cout << "Message: " << topic1.message() << " cu variabila FAIL: " << topic1.fail() << "- TRUE" << " RECEIVED" << std::endl;
					
					gpioWrite(16, 1);
				
					std::ofstream fisier2("outputSubscriberFail.txt", std::ios::app);
            		if(fisier2.is_open())
            		{
                		fisier2 << "Message: " << topic1.message() << " cu variabila FAIL: " << topic1.fail() << "- TRUE" << " RECEIVED" << std::endl;
                		fisier2.close();
            		}
            		else
					{
                			std::cerr << "Eroare la deschiderea fisierului! " << std::endl;
            		}
                }
    		}
        
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));//100
        }

	gpioWrite(16, 0);
	gpioTerminate();
    return 0;
}
