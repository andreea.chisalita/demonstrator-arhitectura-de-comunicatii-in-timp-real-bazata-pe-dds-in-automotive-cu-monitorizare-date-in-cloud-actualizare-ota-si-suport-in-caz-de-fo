#include <thread>
#include <pigpio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <chrono>


void configurareDDS()
{
	
	topic1.fail(1);
    topic1.message(" XMC DOWN! SISTEMUL A INTRAT IN MODUL FAIL OPERATION!");
	
    eprosima::fastrtps::ParticipantAttributes participant_attr1; 
	Participant *participant1 = Domain::createParticipant(participant_attr1);
	
	PublisherAttributes publisher_attr1;
	
	publisher_attr1.topic.topicName = "topicFailOperationTopic";
	
	publisher4 = Domain::createPublisher(participant1, publisher_attr1);

}


int main(int argc, char** argv)
{
    std::cout << "Starting publisher." << std::endl;
    int samples = 10;

    configurareDDS();
     while (samples)
        {	
    		if(publisher4->write(&topic1))
            {
            	std::cout << "Message: " << topic1.message() << " cu variabila FAIL: " << topic1.fail() << "- TRUE" << " SENT" << std::endl;
                std::ofstream fisier3("outputPublisherFail.txt", std::ios::app);
            	if(fisier3.is_open())
            	{
                	fisier3 << "Message: " << topic1.message() << " cu variabila FAIL: " << topic1.fail() << "- TRUE" << " SENT" << std::endl;
                	fisier3.close();
            	}
            	else
				{
                	std::cerr << "Eroare la deschiderea fisierului! " << std::endl;
            	}
            }
            
            std::this_thread::sleep_for(std::chrono::milliseconds(1000)); //100
        }
    		
    return 0;
}