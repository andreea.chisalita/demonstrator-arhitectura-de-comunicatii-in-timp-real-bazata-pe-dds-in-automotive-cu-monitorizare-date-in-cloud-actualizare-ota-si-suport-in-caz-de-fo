#include <thread>
#include "SampleInfo.h"
#include <pigpio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <chrono>


void configurareDDS()
{

	topic.timestamp(0);
    topic.message("DEFAULT VALUE");
	std::cout << "INITIAL Message " << topic.message() << " " << topic.timestamp() << " INITIAL TIMESTAMP" << std::endl;

    eprosima::fastrtps::ParticipantAttributes participant_attr; 
	Participant *participant = Domain::createParticipant(participant_attr);

	SubscriberAttributes subscriber_attr;
	
	subscriber_attr.topic.topicName = "topicDirectieVitezaTopic";
	subscriber3 = Domain::createSubscriber(participant, subscriber_attr);

}

int main(int argc, char** argv)
{
    std::cout << "Starting subscriber." << std::endl;
    int samples = 10;
	bool xmcDown = false;

    configurareDDS();
    while(samples)
        {
        
        	if (subscriber3->takeNextData((void*)&topic, &m_info))
    		{
         		if (m_info.sampleKind == eprosima::fastrtps::rtps::ALIVE)
        		{
            			std::cout << topic.timestamp() << " ms" << " " << topic.message() << " RECEIVED" << std::endl;

						std::ofstream fisier1("outputSubscriber.txt", std::ios::app);
            			if(fisier1.is_open())
            			{
                			fisier1 << topic.timestamp() << " ms" << " " << topic.message() << " RECEIVED" << std::endl;
                			fisier1.close();
            			}
            			else{
                			std::cerr << "Eroare la deschiderea fisierului! " << std::endl;
            			}
					
						if(topic.message().find("XMC DOWN!!! FAIL OPERATION!! ") != std::string::npos)
						{
							std::cout << "S-a detectat ca am intrat in modul FAIL OPERATION! A cedat XMC-ul! Programul se inchide in 3 secunde..." << std::endl;
							xmcDown = true;
							break; 
						}
        		}
    		}
        
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));//100
        }

	if(xmcDown == true)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(3000));
		system("/home/iasmina/Desktop/arhiva/IASMINA/Epr_DDS/DDS_Examples/build/startPublisher2.sh"); 
	}

    return 0;
}
