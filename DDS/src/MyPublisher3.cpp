#include <thread>
#include <pigpio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <chrono>

void configurareDDS()
{
	
	topic2.rec(1);
    topic2.message(" XMC SCHIMBA DIRECTIA POTENTIOMETRELOR! SISTEMUL A INTRAT IN MODUL DE RECALIBRARE!");

    eprosima::fastrtps::ParticipantAttributes participant_attr2; 
	Participant *participant2 = Domain::createParticipant(participant_attr2);
	
	PublisherAttributes publisher_attr2;
	
	publisher_attr2.topic.topicName = "topicRecalibrareTopic";
	
	publisher5 = Domain::createPublisher(participant2, publisher_attr2);

}


int main(int argc, char** argv)
{
    std::cout << "Starting publisher." << std::endl;
    int samples = 10;

    configurareDDS();
     while (samples)
        {	
    		if(publisher5->write(&topic2))
            {
            	std::cout << "Message: " << topic2.message() << " cu variabila flag RECALIBRARE: " << topic2.rec() << "- TRUE" << " SENT" << std::endl;
                std::ofstream fisier4("outputPublisherRec.txt", std::ios::app);
            	if(fisier4.is_open())
            	{
                	fisier4 << "Message: " << topic2.message() << " cu variabila flag RECALIBRARE: " << topic2.rec() << "- TRUE" << " SENT" << std::endl;
                	fisier4.close();
            	}
            	else
				{
                	std::cerr << "Eroare la deschiderea fisierului! " << std::endl;
            	}
            }
            
            std::this_thread::sleep_for(std::chrono::milliseconds(100)); //100
        }
    		
    return 0;
}