#include <thread>
#include <pigpio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <chrono>

unsigned long timestamp = 0;

void configurareDDS()
{
	
	topic.timestamp(0);
    topic.message("INFORMATION");
	
    eprosima::fastrtps::ParticipantAttributes participant_attr;
	Participant *participant = Domain::createParticipant(participant_attr);
	
	PublisherAttributes publisher_attr;
	
	publisher_attr.topic.topicName = "topicDirectieVitezaTopic";
	
	publisher3 = Domain::createPublisher(participant, publisher_attr);

}

void publish_message(const std::string& message, const unsigned long& timestamp)
{
    if(publisher3)
    {
        topic.timestamp(timestamp);
        topic.message(message.c_str());
        if(publisher3->write(&topic))
        {
            std::cout << topic.timestamp() << " ms" << " " << topic.message() << " SENT" << std::endl;

            std::ofstream fisier("outputPublisher.txt", std::ios::app);
            if(fisier.is_open())
            {
                fisier << topic.timestamp() << " ms" << " " << topic.message() << " SENT" << std::endl;
                fisier.close();
            }
            else{
                std::cerr << "Eroare la deschiderea fisierului! " << std::endl;
            }
        }
    }
}

int main(int argc, char** argv)
{
    std::cout << "Starting publisher." << std::endl;
    int samples = 10;

    if(gpioInitialise() < 0)
    {
        std::cerr << "Failed to initialize GPIO\n";
        return 1;
    }

    gpioSetMode(17, PI_INPUT);
    gpioSetMode(27, PI_INPUT);
    gpioSetMode(22, PI_INPUT);
    gpioSetMode(23, PI_INPUT);
    gpioSetMode(24, PI_INPUT);
    gpioSetMode(25, PI_INPUT);

    configurareDDS();

    while(samples)
    {
        std::string message;
       
        if(gpioRead(17) == 0)
        {
            message += "XMC stanga, ";
        }
        else
        {
            message += "XMC dreapta, ";
        }

        if(gpioRead(27) == 0)
        {
            message += "viteza mica; ";
        }
        else
        {
            message += "viteza mare; ";
        }

        if(gpioRead(22) == 1)
        {
            message += "STM motor rotatie stanga cu viteza mica; ";
        }
       
        if(gpioRead(23) == 1)
        {
            message += "STM motor rotatie stanga cu viteza mare; ";
        }

        if(gpioRead(24) == 1)
        {
            message += "STM motor rotatie dreapta cu viteza mica; ";
        }

        if(gpioRead(25) == 1)
        {
            message += "STM motor rotatie dreapta cu viteza mare; ";
        }

        timestamp = timestamp + 1000; 
        publish_message(message,timestamp);
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    gpioTerminate();
    return 0;
}

