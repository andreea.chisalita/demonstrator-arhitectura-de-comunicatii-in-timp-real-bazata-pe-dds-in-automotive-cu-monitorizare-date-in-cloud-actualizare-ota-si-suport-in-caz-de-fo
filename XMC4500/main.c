
#include <DAVE.h>

#include "Dave/Generated/FREERTOS/task.h"
#include "Dave/Generated/FREERTOS/queue.h"
#include "Dave/Generated/FREERTOS/FreeRTOS.h"
#include "Dave/Generated/FREERTOS/semphr.h"


// Prototipuri de funcții
void AdcReadTask(void *pvParameters);
void LedControlTask(void *pvParameters);

// Variabile globale pentru stocarea valorilor ADC
volatile uint32_t adcValue0 = 0;
volatile uint32_t adcValue1 = 0;
//portTickType xNextWakeTime, xNextWakeTime2;
int main(void) {
    DAVE_STATUS_t status;

    status = DAVE_Init();  // Initializarea aplicatiilor DAVE

    if (status != DAVE_STATUS_SUCCESS) {
        XMC_DEBUG("DAVE APPs initialization failed\n");
        while (1U);
    }
    ADC_MEASUREMENT_StartConversion(&ADC_MEASUREMENT_0);
    // Crearea task-urilor FreeRTOS
    xTaskCreate(AdcReadTask, "ADC Read", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, NULL);
    xTaskCreate(LedControlTask, "LED Control", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, NULL);

    // Pornirea schedulerului FreeRTOS
    vTaskStartScheduler();

    while (1U);
}

// Task pentru citirea ADC
void AdcReadTask(void *pvParameters) {
	portTickType xNextWakeTime;
	xNextWakeTime = xTaskGetTickCount();
    while (1) {
        adcValue0 = ADC_MEASUREMENT_GetResult(&ADC_MEASUREMENT_Channel_A) / 0.4095;
        adcValue1 = ADC_MEASUREMENT_GetResult(&ADC_MEASUREMENT_Channel_B) / 0.4095;

        //vTaskDelay(pdMS_TO_TICKS(100));  // Întârziere de 100 ms
        vTaskDelayUntil( &xNextWakeTime, pdMS_TO_TICKS(100));
    }
}

// Task pentru controlul LED-urilor și setarea PWM
void LedControlTask(void *pvParameters) {
	portTickType xNextWakeTime2;
	xNextWakeTime2 = xTaskGetTickCount();
    while (1) {
        uint32_t value = adcValue0;
        uint32_t value1 = adcValue1;
        uint8_t flagRECALIBRARE = DIGITAL_IO_GetInput(&RECALIBRARE);

        PWM_SetDutyCycle(&PWM_0, value);
        PWM_SetDutyCycle(&PWM_1, value1);

        if(flagRECALIBRARE == 0)
        {

			if (value < 6000)
			{
				DIGITAL_IO_SetOutputLow(&LED0);
				DIGITAL_IO_SetOutputHigh(&ENABLE);
			}
			else
			{
				DIGITAL_IO_SetOutputHigh(&LED0);
				DIGITAL_IO_SetOutputHigh(&ENABLE);
			}

			if (value1 < 6000)
			{
				DIGITAL_IO_SetOutputLow(&LED1);
				DIGITAL_IO_SetOutputHigh(&ENABLE);
			}
			else
			{
				DIGITAL_IO_SetOutputHigh(&LED1);
				DIGITAL_IO_SetOutputHigh(&ENABLE);
			}
        }
        else
        {
        	if (value < 6000)
        		{
        			DIGITAL_IO_SetOutputHigh(&LED0);
        			DIGITAL_IO_SetOutputHigh(&ENABLE);
        		}
        	else
        		{
        			DIGITAL_IO_SetOutputLow(&LED0);
        			DIGITAL_IO_SetOutputHigh(&ENABLE);
        		}

        	if (value1 < 6000)
        		{
        			DIGITAL_IO_SetOutputHigh(&LED1);
        			DIGITAL_IO_SetOutputHigh(&ENABLE);
        		}
        	else
        		{
        			DIGITAL_IO_SetOutputLow(&LED1);
        			DIGITAL_IO_SetOutputHigh(&ENABLE);
        		}
        }

        //vTaskDelay(pdMS_TO_TICKS(150));  // Întârziere de 150 ms
        vTaskDelayUntil( &xNextWakeTime2, pdMS_TO_TICKS(150));
    }
}

