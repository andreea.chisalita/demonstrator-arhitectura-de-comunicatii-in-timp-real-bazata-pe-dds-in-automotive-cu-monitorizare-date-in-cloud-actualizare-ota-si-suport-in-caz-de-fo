# Demonstrator arhitectură de comunicații în timp real bazată pe DDS în automotive, cu monitorizare date în Cloud, actualizare Over-The-Air și suport în caz de Fail Operation

Pașii ce trebuie urmăriți pentru a rula scenariile proiectului:

## XMC4500

Pasul 1: Instalarea IDE-ului Dave, plus configurarea și instalarea driverelelor SEGGER J-Link de pe site-ul: https://www.infineon.com/cms/en/product/promopages/dave3-download/.

Pasul 2: Crearea unui proiect, configurarea pinilor utilizați (prezentați în documentație) și a componentelor utilizate, pentru a crea App Dependency Tree-ul prezentat.

Pasul 3: După generarea automată a codului inițial, introducerea codului din /XMC4500/main.c în /src/main.c.

Pasul 4: Build-uirea proiectului.

Pasul 5: Conectarea XMC4500 prin USB. 

Pasul 6: Selectarea Run > Debug Configurations, configurarea GDB SEGGER J-Link Debugging și apăsarea butonului de Debug pentru încărcarea codului pe placă și folosirea funcționalităților de debugging din DAVE pentru a seta breakpoints și pentru a monitoriza variabilele.

## Ubuntu pe Virtual Machine

Pasul 1: Instalarea VirtualBox conform instrucțiunilor de pe site-ul oficial.

Pasul 2: Instalarea și configurarea Ubuntu pe VirtualBox.

Pasul 3: Instalarea pe Ubuntu VM a Visual Studio Code de pe site-ul: https://code.visualstudio.com/download.

## STM32L476RG

Pasul 1: Instalarea pe Ubuntu VM a STM32CubeMX și STM32CubeIDE.

Pasul 2: Configurarea tipului de microcontroler folosit, STM32L476RG, în STMCubeMX și a pinilor și componentelor utilizate.

Pasul 3: Generarea codului inițial prin STM32CubeMX și pornirea STM32CubeIDE.

Pasul 4: Inserarea codului în /Core/Src/main.c.

Pasul 5: Conectarea plăcii de dezvoltare STM32L476RG la PC folosind un cablu USB.

Pasul 6: Selectarea Run > Debug Configurations și asigurarea că ST-LINK (OpenOCD) este selectat și configurat corect.

Pasul 7: Apăsarea pe butonul Build pentru a compila proiectul și verificarea dacă nu există erori de compilare.

Pasul 8: Apăsarea pe butonul Run sau Debug pentru a flash-ui codul pe placa STM32L476RG.

Pasul 9: Folosirea funcționalităților de debugging din STM32CubeIDE pentru a seta breakpoints, a urmări variabile și a executa pas cu pas codul


## Implementare DDS pe Ubuntu și Raspberry Pi 3 Model B

Pasul 1: Instalarea pigpio pe Raspberry Pi. 

Pasul 2: Urmărirea cu atenție a pașilor de configurare de pe site-ul eProsima: https://fast-dds.docs.eprosima.com/en/latest/.

Pasul 3: Instalarea eProsima Fast DDS și a dependințelor, urmărind instrucțiunile de pe site.

Pasul 4: Configurarea topic-urilor în /src conform codurilor topicDirectieViteza.idl, topicRecalibrare.idl și topicFailOperation.idl.

Pasul 5: Generarea fișierelor necesare executând comenzile de pe site ce conțin calea către fișierele .idl.

Pasul 6: Configurarea în /src a codurilor pentru Publisheri și Subscriberi conform codurilor MyPublisher1.cpp, MyPublisher2.cpp, MyPublisher3.cpp și MySubscriber1.cpp, MySubscriber2.cpp, MySubscriber3.cpp.

Pasul 7: Instalarea CMake atât pe Ubuntu VM, cât și pe Raspberry Pi 3.

Pasul 8: Crearea fișierului CMakeLists.txt, conform fișierului cu același nume din repository și conform indicațiilor de pe eProsima.

Pasul 9: Executarea fișierului CMakeLists.txt pentru implementarea executabilelor. 

Pasul 10: Build-uirea codurilor.

Pasul 11: Rularea executabilelor, în funcție de scenariul ales, pe VM Ubuntu și Raspberry Pi 3.
