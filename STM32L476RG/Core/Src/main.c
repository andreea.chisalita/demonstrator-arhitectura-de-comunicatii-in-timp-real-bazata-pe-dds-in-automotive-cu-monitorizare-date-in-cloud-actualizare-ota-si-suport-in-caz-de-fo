/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include "tim.h"
#include "gpio.h"
#include "FreeRTOS.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void MX_FREERTOS_Init(void);
/* USER CODE BEGIN PFP */


/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/*#define DIR_PIN GPIO_PIN_1
#define DIR_PORT GPIOC
#define STEP_PIN GPIO_PIN_2
#define STEP_PORT GPIOC
int stepDelay = 1000; // 1000us more delay means less speed

void microDelay (uint16_t delay)
{
  __HAL_TIM_SET_COUNTER(&htim1, 0);
  while (__HAL_TIM_GET_COUNTER(&htim1) < delay);
}*/
#define DIR_PIN GPIO_PIN_1
#define DIR_PORT GPIOC
#define STEP_PIN GPIO_PIN_2
#define STEP_PORT GPIOC

#define DIR_PIN2 GPIO_PIN_0
#define DIR_PORT2 GPIOC
#define STEP_PIN2 GPIO_PIN_3
#define STEP_PORT2 GPIOC

uint8_t vit=0, dir=0; /*variabilele ce vin de la XMC*/

uint8_t vit2=0, dir2=0; /*variabilele ce vin de la XMC*/

//uint8_t fail, fail2;

void microDelay (uint16_t delay)
{
  __HAL_TIM_SET_COUNTER(&htim1, 0);
  while (__HAL_TIM_GET_COUNTER(&htim1) < delay);
}

void step (int steps, uint8_t direction, uint16_t delay)
{
  int x;
  if (direction == 0)
    HAL_GPIO_WritePin(DIR_PORT, DIR_PIN, GPIO_PIN_SET);
  else
    HAL_GPIO_WritePin(DIR_PORT, DIR_PIN, GPIO_PIN_RESET);
  for(x=0; x<steps; x=x+1)
  {
    HAL_GPIO_WritePin(STEP_PORT, STEP_PIN, GPIO_PIN_SET);
    microDelay(delay);
    HAL_GPIO_WritePin(STEP_PORT, STEP_PIN, GPIO_PIN_RESET);
    microDelay(delay);
  }
}

void step2 (int steps, uint8_t direction, uint16_t delay)
{
  int x;
  if (direction == 0)
    HAL_GPIO_WritePin(DIR_PORT2, DIR_PIN2, GPIO_PIN_SET);
  else
    HAL_GPIO_WritePin(DIR_PORT2, DIR_PIN2, GPIO_PIN_RESET);
  for(x=0; x<steps; x=x+1)
  {
    HAL_GPIO_WritePin(STEP_PORT2, STEP_PIN2, GPIO_PIN_SET);
    microDelay(delay);
    HAL_GPIO_WritePin(STEP_PORT2, STEP_PIN2, GPIO_PIN_RESET);
    microDelay(delay);
  }
}

uint8_t readDebouncedPin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin) {
    uint8_t currentState = HAL_GPIO_ReadPin(GPIOx, GPIO_Pin);
    osDelay(6);
    uint8_t nextState = HAL_GPIO_ReadPin(GPIOx, GPIO_Pin);
    if (currentState == nextState) {
        return currentState; // Returnează starea dacă este stabilă
    }
    return 0xFF; // Sau altă valoare care indică o citire nesigură
}

void controlMOTOR(void){
	/*aici implementez functie*/
	//dir = HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0);
	//vit = HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_1);
	dir = readDebouncedPin(GPIOA, GPIO_PIN_0);
	vit = readDebouncedPin(GPIOA, GPIO_PIN_1);
	uint8_t enable = readDebouncedPin(GPIOA, GPIO_PIN_4);
	uint8_t fail = readDebouncedPin(GPIOC, GPIO_PIN_6);

	static uint8_t nrCiclu = 0;
	static uint8_t fazaCurenta = 0;


	if (fail == 1)
	{
	    if (nrCiclu < 200)
	    {
	        switch (fazaCurenta)
	        {
	            case 0:
	                step(20, 0, 2500);
	                HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_SET);
	                HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_RESET);
	                HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_RESET);
	                HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11, GPIO_PIN_RESET);
	                break;
	            case 1:
	                step(20, 0, 1000);
	                HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_RESET);
	                HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_SET);
	                HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_RESET);
	                HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11, GPIO_PIN_RESET);
	                break;
	            case 2:
	                step(20, 1, 2500);
	                HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_RESET);
	                HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_RESET);
	                HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_SET);
	                HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11, GPIO_PIN_RESET);
	                break;
	            case 3:
	                step(20, 1, 1000);
	                HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_RESET);
	                HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_RESET);
	                HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_RESET);
	                HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11, GPIO_PIN_SET);
	                break;
	         }
	         nrCiclu++;
	    } else {
	         nrCiclu = 0;
	         fazaCurenta= (fazaCurenta + 1) % 4;  // trec la urmatoarea faza
	       }
	}
	if(enable == 1)
	{
	if(dir == 0){
		if(vit == 0){
			//directie stanga, viteza mica
			step(20,0,2500);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_SET);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11, GPIO_PIN_RESET);
		}
	}

	if(dir == 0){
		if(vit == 1){
			//directie stanga, viteza mare
			step(20,0,1000);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_SET);
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11, GPIO_PIN_RESET);
		}
	}

	if(dir == 1) {
			if(vit == 0){
				//directie dreapta, viteza mica
				step(20,1,2500);
				HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_SET);
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11, GPIO_PIN_RESET);
			}
		}
	if(dir == 1){
			if(vit == 1){
				//directie dreapta, viteza mare
				step(20,1,1000);
				HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11, GPIO_PIN_SET);
				}
			}
	}
	//else if((dir == 0) && (vit == 0) && (enable == 0))
	else if(enable == 0)
	{
		step(0,0,0);
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11, GPIO_PIN_RESET);

	}

}

void controlMOTOR2 (void){
	/*aici implementez functie*/
	//dir2 = HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0);
	//vit2 = HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_1);
	dir2 = readDebouncedPin(GPIOA, GPIO_PIN_0);
	vit2 = readDebouncedPin(GPIOA, GPIO_PIN_1);
	uint8_t enable2 = readDebouncedPin(GPIOA, GPIO_PIN_4);
	uint8_t fail2 = readDebouncedPin(GPIOC, GPIO_PIN_6);

	static uint8_t nrCiclu2 = 0;
    static uint8_t fazaCurenta2 = 0;


    if (fail2 == 1)
    {
        if (nrCiclu2 < 200) {
            switch (fazaCurenta2) {
                case 0:
                    step2(20, 0, 2500);
                    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_SET);
                    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_RESET);
                    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_RESET);
                    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11, GPIO_PIN_RESET);
                    break;
                case 1:
                    step2(20, 0, 1000);
                    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_RESET);
                    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_SET);
                    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_RESET);
                    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11, GPIO_PIN_RESET);
                    break;
                case 2:
                    step2(20, 1, 2500);
                    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_RESET);
                    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_RESET);
                    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_SET);
                    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11, GPIO_PIN_RESET);
                    break;
                case 3:
                    step2(20, 1, 1000);
                    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_RESET);
                    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_RESET);
                    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_RESET);
                    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11, GPIO_PIN_SET);
                    break;
            }
            nrCiclu2++;
    } else {
            nrCiclu2 = 0;
            fazaCurenta2= (fazaCurenta2 + 1) % 4;  // trec la urmatoarea faza
    	}
    }
	if(enable2 == 1)
	{
	if(dir2 == 0) {
		if(vit2 == 0){
			//directie stanga, viteza mica
			step2(20,0,2500);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_SET);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11, GPIO_PIN_RESET);
		}
	}

	if(dir2 == 0){
		if(vit2 == 1){
			//directie stanga, viteza mare
			step2(20,0,1000);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_SET);
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11, GPIO_PIN_RESET);
		}
	}

	if(dir2 == 1){
			if(vit2 == 0){
				//directie dreapta, viteza mica
				step2(20,1,2500);
				HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_SET);
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11, GPIO_PIN_RESET);
			}
		}
	if(dir2 == 1){
			if(vit2 == 1){
				//directie dreapta, viteza mare
				step2(20,1,1000);
				HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11, GPIO_PIN_SET);
				}
			}
	}
	//else if((dir2 == 0) && (vit2 == 0) && (enable2 == 0))
	else if (enable2 == 0)
		{
			step2(0,0,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11, GPIO_PIN_RESET);
		}
}

void myTask1(void *pvParameters){
	//HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, GPIO_PIN_SET);
	//fail = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_6);


	while(1){
		/* aici logica pt task-ul 1 si apelul functiilor folosite
		 */
		controlMOTOR(); /*face toggle la fiecare apel */
		//vTaskDelay(pdMS_TO_TICKS(1000)); /*Asteapta 1 s = 1000ms */
		vTaskDelay(pdMS_TO_TICKS(50));
	}

}

void myTask2(void *pvParameters){
	//HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, GPIO_PIN_SET);
	//fail2 = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_6);
	//if(fail2 == 0)
	//{
	while(1){
		/* aici logica pt task-ul 1 si apelul functiilor folosite
		 */
		controlMOTOR2(); /*face toggle la fiecare apel */
		//vTaskDelay(pdMS_TO_TICKS(1000)); /*Asteapta 1 s = 1000ms */
		vTaskDelay(pdMS_TO_TICKS(50));
	}
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM1_Init();
  /* USER CODE BEGIN 2 */
  HAL_TIM_Base_Start(&htim1);
  /* USER CODE END 2 */

  /* Call init function for freertos objects (in freertos.c) */
  MX_FREERTOS_Init();

  //enable = readDebouncedPin(GPIOA, GPIO_PIN_4);
  //fail = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_6);


  xTaskCreate(myTask1, "MotorTask", configMINIMAL_STACK_SIZE, NULL, 1, NULL);
  xTaskCreate(myTask2, "MotorTask2", configMINIMAL_STACK_SIZE, NULL, 1, NULL);


  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {


	   /* int x;
	    HAL_GPIO_WritePin(DIR_PORT, DIR_PIN, GPIO_PIN_SET);
	    for(x=0; x<200; x=x+1)
	    {
	      HAL_GPIO_WritePin(STEP_PORT, STEP_PIN, GPIO_PIN_SET);
	      microDelay(stepDelay);
	      HAL_GPIO_WritePin(STEP_PORT, STEP_PIN, GPIO_PIN_RESET);
	      microDelay(stepDelay);
	    }
	    HAL_Delay(1000);
	    HAL_GPIO_WritePin(DIR_PORT, DIR_PIN, GPIO_PIN_RESET);
	    for(x=0; x<200; x=x+1)
	    {
	      HAL_GPIO_WritePin(STEP_PORT, STEP_PIN, GPIO_PIN_SET);
	      microDelay(stepDelay);
	      HAL_GPIO_WritePin(STEP_PORT, STEP_PIN, GPIO_PIN_RESET);
	      microDelay(stepDelay);
	    }
	    HAL_Delay(1000);*/

	  	  /*
	  	   * aici e ok
	  	   int y;
	      for(y=0; y<8; y=y+1) // 8 times
	      {
	        step(25, 0, 800); // 25 steps (45 degrees) CCV
	        HAL_Delay(500);
	      }
	      step(800, 1, 5000); // 800 steps (4 revolutions ) CV 5000 - LESS SPEED, 1000-MORE SPEED
	      HAL_Delay(1000); */


    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = 0;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 36;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM2 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM2) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
